package com.file;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.FileReader;

public class ReadCsvFile
{
    // static block,
    static
    {
        System.out.println("\n*****FileReader,********\n");
    }

    public static void main(String[] args)
    {
        // 1. Open file Reader,
        try(BufferedReader reader = new BufferedReader(new FileReader("test1.csv")))
        {
            String line;

            // 2. Read line by line
                while ((line=reader.readLine()) !=null)
                {
                    try {
                        // 3. split the lines,
                        String[] values = line.split(",");

                        // 4. store the values,
                        String name = values[0];
                        int age = Integer.parseInt(values[1]);
                        double salary = Double.parseDouble(values[2]);

                        // 5. process,
                        System.out.println("Name" + ":-" + name);
                        System.out.println("Age" + ":-" + age);
                        System.out.println("Salary" + ":-" + salary);
                    }
                    catch (Exception e)
                    {
                        System.out.println(e.getCause());
                    }
                }
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
        finally
        {
            System.out.println("Completed,");
        }
    }
}
