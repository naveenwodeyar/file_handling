package com.file;

public class Blocks
{
    static
    {
        System.out.println("Static Block");
    }

    public static void main(String[] args)
    {
        System.out.println("*************"+new Blocks());
    }

    {
        System.out.println("\nInstance Block\n");
    }
}
